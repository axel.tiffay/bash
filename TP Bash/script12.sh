#!/bin/bash
#Auteur : Axel Tiffay 4B

echo "Evil or demon"
echo "Voulez-vous l'empreinte ou le nom du fichier ? (e ou f)"
read answer
if [ $answer = "e" ]
then
  while read line
  do
    echo $line | awk '{print $1}' >> pathOnly.txt
  done < empreintes.txt
fi

if [ $answer = "f" ]
then
  while read line
  do
    echo $line | awk '{print $2}' >> pathOnly.txt
  done < empreintes.txt
fi