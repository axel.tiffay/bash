#!/bin/bash
#Auteur : Axel Tiffay 4B

echo "Bienvenue, ce sript permet d'installer une multitude de script sur un ordinateur utilisant linux"
echo "Quelle programme souhaitez-vous installer ?"
echo "Ecrivez (j) pour java"
echo "(c) pour visual studio code"
echo "(p) pour python" 
echo "(doc) pour docker"
echo "(discord) pour discord"
echo "(spotify) pour spotify"
echo "(v) pour VLC"
echo "(q) pour QUITTER <------------------------------------------------------"

read reponse
if [ $reponse = "c" ]
then 
    sudo apt update > /dev/null
    sudo apt install snap > /dev/null
    sudo snap install --classic code
    code --install-extension ms-python.python
    code --install-extension njpwerner.autodocstring
    code --install-extension ms-ceintl.vscode-language-pack-fr
    code --install-extension gitlab.gitlab-workflow
    code --install-extension cameron.vscode-pytest
    reset
    echo " "
    echo " "
    echo "installation de code REUSSIE"
    echo " "
    echo " "
    sleep 4
    sh ./bash.sh
fi

if [ $reponse = "p" ]
then 
    sudo apt update > /dev/null
    sudo apt install python3 -y
    sudo apt install pylint -y
    sudo pip install pytest-3 -y
    echo " "
    echo " "
    echo "python3 pylint installation REUSSIE !"
    echo " "
    echo " "
    echo "Souhaitez-vous lancer le script custom print(\"hello world\") ? (y/n)"
    read reponse
    if [ $reponse = "y" ]
    then
        reset
        echo " "
        echo " "
        python3 -c 'print("Hello world !")'
        echo " "
        echo " "
    fi
    sleep 4
    sh ./bash.sh
fi

if [ $reponse = "j" ]
then
    sudo apt install default-jdk -y
    java -version
    echo " "
    echo " "
    echo "java jre installation REUSSIE !"
    echo " "
    echo " "
    echo "Souhaitez-vous lancer le script custom java print (\"hello world\") ? (y/n)"
    read reponse
    if [ $reponse = "y" ]
    then
        reset
        echo " "
        echo " "
        touch Helloworld.java
        cat <<EOF >  Helloworld.java
 public class Helloworld{
     public static void main(String[] args){
            System.out.println(" Hello World !") ;
       }
}
EOF
        javac Helloworld.java
        java Helloworld
        echo " "
        echo " "
    fi
    sleep 4
    sh ./bash.sh
fi


if [ $reponse = "doc" ]
then
    sudo apt update
	sudo apt install apt-transport-https ca-certificates curl software-properties-common
	sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add –
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
	sudo apt update
	sudo apt install docker-ce -y
	sudo apt install docker.io -y
	sudo docker run hello-world
    echo " "
    echo " "
    echo "DOCKER installation REUSSIE !"
    echo " "
    echo " "
    sleep 4
    sh ./bash.sh
fi

if [ $reponse = "discord" ]
then
    sudo apt install snap > /dev/null
    sudo snap install discord
    reset
    echo " "
    echo " "
    echo "DISCORD installation REUSSIE !"
    echo " "
    echo " "
    sleep 4
    sh ./bash.sh
fi

if [ $reponse = "spotify" ]
then
    sudo apt install snap > /dev/null
    sudo snap install spotify
    reset
    echo " "
    echo " "
    echo "SPOTIFY installation REUSSIE !"
    echo " "
    echo " "
    sleep 4
    sh ./bash.sh
fi

if [ $reponse = "v" ]
then
    sudo apt install snap > /dev/null
    sudo snap install vlc
    reset
    echo " "
    echo " "
    echo "VLC installation REUSSIE !"
    echo " "
    echo " "
    sleep 4
    sh ./bash.sh
fi

if [ $reponse = "q" ]
then    
    echo "Nous quittons immédiatement le script"
fi


